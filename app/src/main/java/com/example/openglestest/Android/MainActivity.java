package com.example.openglestest.Android;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity {

    private OpenGLSurfaceView mGLView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGLView = new OpenGLSurfaceView(this);
        mGLView.setEGLContextClientVersion(2);

        OpenGLRenderer renderer = new OpenGLRenderer(this);
		mGLView.setRenderer(renderer);
        setContentView(mGLView);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGLView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGLView.onResume();
    }
}
