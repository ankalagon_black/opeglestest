package com.example.openglestest.Android;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

public class OpenGLSurfaceView extends GLSurfaceView {
    
	private OpenGLRenderer renderer;

    private float lastX, lastY;

    private float lastDistance;

    private boolean isScaling;

    public OpenGLSurfaceView(Context context) {
        super(context);
        isScaling = false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        int firstPointer = e.getPointerId(0), firstPointerIndex = e.findPointerIndex(firstPointer);
        float x = e.getX(firstPointerIndex), y = e.getY(firstPointerIndex);

        switch (e.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_MOVE:
                if(isScaling){
                    int secondPointer = e.getPointerId(1), secondPointerIndex = e.findPointerIndex(secondPointer);
                    float x2 = e.getX(secondPointerIndex), y2 = e.getY(secondPointerIndex);
                    float diffX = (x2 - x) / 40.0f, diffY = (y2 -y) / 40.0f;
                    float distance = Math.abs(diffX + diffY);
                    if(lastDistance > 0){
                        renderer.addDistance(lastDistance - distance);
                        requestRender();
                    }
                    lastDistance = distance;
                }
                else{
                    float dx = x - lastX;
                    float dy = y - lastY;
                    if(Math.abs(dx) < 50.0f) renderer.addYRotation(dx * 0.5f);
                    if(Math.abs(dy) < 50.0f) renderer.addZRotation(dy * 0.2f);
                    requestRender();
                    lastX = x;
                    lastY = y;
                }
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                isScaling = true;
                lastDistance = -1;
                break;
            case MotionEvent.ACTION_POINTER_UP:
                isScaling = false;
                break;
        }
        return true;
    }
    
    public void setRenderer(OpenGLRenderer renderer) {
        super.setRenderer(renderer);
        this.renderer = renderer;
    }
}
