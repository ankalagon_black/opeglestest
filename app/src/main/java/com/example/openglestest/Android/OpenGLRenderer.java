package com.example.openglestest.Android;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;

import com.example.openglestest.FramebufferProperties.OpenGLProgram;
import com.example.openglestest.FramebufferProperties.SimpleFramebufferWrapper;
import com.example.openglestest.Objects.Cube;
import com.example.openglestest.Objects.Renderable;
import com.example.openglestest.R;
import com.example.openglestest.SceneProperties.Camera;
import com.example.openglestest.SceneProperties.ProjectionWrapper;

import java.util.ArrayList;
import java.util.List;

public class OpenGLRenderer implements GLSurfaceView.Renderer {

    private static final String TAG = "OpenGLRenderer";

    private final MainActivity mainActivity;

	private Cube cube;

	private ProjectionWrapper projectionWrapper;

	private Camera camera;

	private SimpleFramebufferWrapper simpleFramebufferWrapper;

	public OpenGLRenderer(final MainActivity shadowsActivity) {
		mainActivity = shadowsActivity;
	}
	
    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
		GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		GLES20.glEnable(GLES20.GL_DEPTH_TEST);
		GLES20.glEnable(GLES20.GL_CULL_FACE);

		camera = new Camera(5.0f, 0.0f, 0.0f);

		cube = new Cube(2.0f);

		OpenGLProgram openGLProgram = new OpenGLProgram(R.raw.simple_v, R.raw.simple_f, mainActivity);

		List<Renderable> renderables = new ArrayList<>();

		renderables.add(cube);

		simpleFramebufferWrapper = new SimpleFramebufferWrapper(0, openGLProgram, renderables);
//		Matrix.setLookAtM(sceneViewMatrix, 0,
//							cameraPos[0], cameraPos[1], cameraPos[2],
//				lightSourcePos[0], lightSourcePos[1], lightSourcePos[2],
//        					0, 1, 0);
//		Matrix.setLookAtM(lightViewMatrix, 0,
//							lightSourcePos[0], lightSourcePos[1], lightSourcePos[2],
//							0, 0, 0,
//							0, 1, 0);
    }

//	public void generateShadowFBO() {
//		FBOShadowMapId = new int[1];
//		renderShadowMapTextureId = new int[1];
//
//		GLES20.glGenTextures(1, renderShadowMapTextureId, 0);
//		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, renderShadowMapTextureId[0]);
//		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
//		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
//		GLES20.glTexParameteri( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE );
//		GLES20.glTexParameteri( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE );
//
//		GLES20.glGenFramebuffers(1, FBOShadowMapId, 0);
//		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, FBOShadowMapId[0]);
//
//		GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, screenWidth, screenHeight, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_SHORT_4_4_4_4, null);
//		GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, renderShadowMapTextureId[0], 0);
//
//		int FBOstatus = GLES20.glCheckFramebufferStatus(GLES20.GL_FRAMEBUFFER);
//		if(FBOstatus != GLES20.GL_FRAMEBUFFER_COMPLETE) {
//			Log.e(TAG, "Framebuffer creation error.");
//			throw new RuntimeException("Framebuffer creation error.");
//		}
//	}
//
//	public void generateSceneFBO(){
//		FBOSceneId = new int[1];
//		renderSceneTextureId = new int[1];
//
//		int[] depthRenderBuffer = new int[1];
//
//		GLES20.glGenRenderbuffers(1, depthRenderBuffer, 0);
//		GLES20.glBindRenderbuffer(GLES20.GL_RENDERBUFFER, depthRenderBuffer[0]);
//		GLES20.glRenderbufferStorage(GLES20.GL_RENDERBUFFER, GLES20.GL_DEPTH_COMPONENT16, screenWidth, screenHeight);
//
//		GLES20.glGenTextures(1, renderSceneTextureId, 0);
//		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, renderSceneTextureId[0]);
//		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
//		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
//		GLES20.glTexParameteri( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE );
//		GLES20.glTexParameteri( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE );
//
//		GLES20.glGenFramebuffers(1, FBOSceneId, 0);
//		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, FBOSceneId[0]);
//
//		GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, screenWidth, screenHeight, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_SHORT_4_4_4_4, null);
//		GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, renderSceneTextureId[0], 0);
//		GLES20.glFramebufferRenderbuffer(GLES20.GL_FRAMEBUFFER, GLES20.GL_DEPTH_ATTACHMENT, GLES20.GL_RENDERBUFFER, depthRenderBuffer[0]);
//
//		int FBOstatus = GLES20.glCheckFramebufferStatus(GLES20.GL_FRAMEBUFFER);
//		if(FBOstatus != GLES20.GL_FRAMEBUFFER_COMPLETE) {
//			Log.e(TAG, "Framebuffer creation error.");
//			throw new RuntimeException("Framebuffer creation error.");
//		}
//	}
//
//	public void generateOcclusionMapFBO(){
//		FBOOcclusionMapId = new int[1];
//		renderOcclusionMapId = new int[1];
//
//		int[] depthRenderBuffer = new int[1];
//
//		GLES20.glGenRenderbuffers(1, depthRenderBuffer, 0);
//		GLES20.glBindRenderbuffer(GLES20.GL_RENDERBUFFER, depthRenderBuffer[0]);
//		GLES20.glRenderbufferStorage(GLES20.GL_RENDERBUFFER, GLES20.GL_DEPTH_COMPONENT16, screenWidth, screenHeight);
//
//		GLES20.glGenTextures(1, renderOcclusionMapId, 0);
//		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, renderOcclusionMapId[0]);
//		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
//		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
//		GLES20.glTexParameteri( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE );
//		GLES20.glTexParameteri( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE );
//
//		GLES20.glGenFramebuffers(1, FBOOcclusionMapId, 0);
//		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, FBOOcclusionMapId[0]);
//
//		GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, screenWidth, screenHeight, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_SHORT_4_4_4_4, null);
//		GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, renderOcclusionMapId[0], 0);
//		GLES20.glFramebufferRenderbuffer(GLES20.GL_FRAMEBUFFER, GLES20.GL_DEPTH_ATTACHMENT, GLES20.GL_RENDERBUFFER, depthRenderBuffer[0]);
//
//		int FBOstatus = GLES20.glCheckFramebufferStatus(GLES20.GL_FRAMEBUFFER);
//		if(FBOstatus != GLES20.GL_FRAMEBUFFER_COMPLETE) {
//			Log.e(TAG, "Framebuffer creation error.");
//			throw new RuntimeException("Framebuffer creation error.");
//		}
//	}
//

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
		simpleFramebufferWrapper.setWidthAndHeight(width, height);
		projectionWrapper = new ProjectionWrapper(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused) {

		GLES20.glCullFace(GLES20.GL_BACK);

		float[] result = new float[16];

		Matrix.multiplyMM(result, 0, projectionWrapper.getProjectionMatrix(), 0, camera.getViewMatrix(), 0);

		simpleFramebufferWrapper.setProjViewMatrix(result);

		simpleFramebufferWrapper.draw();

		int debugInfo = GLES20.glGetError();
		if (debugInfo != GLES20.GL_NO_ERROR) Log.w(TAG, "OpenGL error: " + debugInfo);
	}

//		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
//		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, renderOcclusionMapId[0]);
//		GLES20.glUniform1i(uOcclusionMapId, 0);
//
//		GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
//		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, renderSceneTextureId[0]);
//		GLES20.glUniform1i(uColorMapId, 1);
//
//		GLES20.glUniform3f(postLightSourcePosId, lightSourcePos[0], lightSourcePos[1], lightSourcePos[2]);


    public void addYRotation(float rotation) {
        camera.addYRotation(rotation);
    }

	public void addZRotation(float rotation){
		camera.addZRotation(rotation);
	}

	public void addDistance(float distance){
		camera.addDistance(distance);
	}

}
