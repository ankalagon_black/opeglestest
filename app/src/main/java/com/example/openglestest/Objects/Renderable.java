package com.example.openglestest.Objects;

/**
 * Created by Константин on 29.12.2016.
 */

public interface Renderable {
    void render(int vertexAttrib, int normalAttrib, int colorAttrib, int modelLocation);
}
