package com.example.openglestest.Objects;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;


import android.opengl.GLES20;

public class Cube extends HaveModel implements Renderable {
	private final FloatBuffer verticesBuffer;
	private final FloatBuffer normalsBuffer;
	private final FloatBuffer colorsBuffer;

	float[] vertices = {
		    -1.0f,-1.0f,-1.0f, //1-1
		    -1.0f,1.0f,-1.0f,
		    1.0f,1.0f,-1.0f,
		    -1.0f,-1.0f,-1.0f, //1-2
		    1.0f,1.0f,-1.0f,
		    1.0f,-1.0f,-1.0f,
		    -1.0f,1.0f,-1.0f, //2-1
		    -1.0f,1.0f,1.0f,
		    1.0f,1.0f,1.0f,
		    -1.0f,1.0f,-1.0f, //2-2
		    1.0f,1.0f,1.0f,
		    1.0f,1.0f,-1.0f,
            1.0f,1.0f,-1.0f, //3-1
            1.0f,1.0f,1.0f,
            1.0f,-1.0f,-1.0f,
            1.0f,-1.0f,-1.0f, // 3-2
            1.0f,1.0f,1.0f,
            1.0f,-1.0f,1.0f,
            -1.0f,-1.0f,-1.0f, // 4-1
            1.0f,-1.0f,1.0f,
            -1.0f,-1.0f,1.0f,
            -1.0f,-1.0f,-1.0f, //4-2
            1.0f,-1.0f,-1.0f,
            1.0f,-1.0f,1.0f,
            -1.0f,-1.0f,-1.0f, //5-1
            -1.0f,1.0f,1.0f,
            -1.0f,1.0f,-1.0f,
            -1.0f,-1.0f,-1.0f, //5-2
            -1.0f,-1.0f,1.0f,
            -1.0f,1.0f,1.0f,
            -1.0f,-1.0f,1.0f, //6-1
            1.0f,1.0f,1.0f,
            -1.0f,1.0f,1.0f,
            -1.0f,-1.0f,1.0f, //6-2
            1.0f,-1.0f,1.0f,
            1.0f,1.0f,1.0f
		};
	
	
	float[] normals = {
            0.0f,0.0f,-1.0f, //1-1
            0.0f,0.0f,-1.0f,
            0.0f,0.0f,-1.0f,
            0.0f,0.0f,-1.0f, //1-2
            0.0f,0.0f,-1.0f,
            0.0f,0.0f,-1.0f,
            0.0f,1.0f,0.0f, //2-1
            0.0f,1.0f,0.0f,
            0.0f,1.0f,0.0f,
            0.0f,1.0f,0.0f, //2-2
            0.0f,1.0f,0.0f,
            0.0f,1.0f,0.0f,
            1.0f,0.0f,0.0f, //3-1
            1.0f,0.0f,0.0f,
            1.0f,0.0f,0.0f,
            1.0f,0.0f,0.0f, //3-2
            1.0f,0.0f,0.0f,
            1.0f,0.0f,0.0f,
            0.0f,-1.0f,0.0f, //4-1
            0.0f,-1.0f,0.0f,
            0.0f,-1.0f,0.0f,
            0.0f,-1.0f,0.0f, //4-2
            0.0f,-1.0f,0.0f,
            0.0f,-1.0f,0.0f,
            -1.0f,0.0f,0.0f, //5-1
            -1.0f,0.0f,0.0f,
            -1.0f,0.0f,0.0f,
            -1.0f,0.0f,0.0f, //5-2
            -1.0f,0.0f,0.0f,
            -1.0f,0.0f,0.0f,
            0.0f,0.0f,1.0f, //6-1
            0.0f,0.0f,1.0f,
            0.0f,0.0f,1.0f,
            0.0f,0.0f,1.0f, //6-2
            0.0f,0.0f,1.0f,
            0.0f,0.0f,1.0f
    };

	float[] colors = {
			1.0f,0.0f,0.0f,1.0f, //1-1
            1.0f,0.0f,0.0f,1.0f,
            1.0f,0.0f,0.0f,1.0f,
            1.0f,0.0f,0.0f,1.0f, //1-2
            1.0f,0.0f,0.0f,1.0f,
            1.0f,0.0f,0.0f,1.0f,
			0.0f,0.0f,1.0f,1.0f, //2-1
            0.0f,0.0f,1.0f,1.0f,
            0.0f,0.0f,1.0f,1.0f,
            0.0f,0.0f,1.0f,1.0f, //2-2
            0.0f,0.0f,1.0f,1.0f,
            0.0f,0.0f,1.0f,1.0f,
            0.0f,1.0f,0.0f,1.0f, //3-1
            0.0f,1.0f,0.0f,1.0f,
            0.0f,1.0f,0.0f,1.0f,
            0.0f,1.0f,0.0f,1.0f, //3-2
            0.0f,1.0f,0.0f,1.0f,
            0.0f,1.0f,0.0f,1.0f,
            0.0f,0.0f,1.0f,1.0f, //4-1
            0.0f,0.0f,1.0f,1.0f,
            0.0f,0.0f,1.0f,1.0f,
            0.0f,0.0f,1.0f,1.0f, //4-2
            0.0f,0.0f,1.0f,1.0f,
            0.0f,0.0f,1.0f,1.0f,
            0.0f,1.0f,0.0f,1.0f, //5-1
            0.0f,1.0f,0.0f,1.0f,
            0.0f,1.0f,0.0f,1.0f,
            0.0f,1.0f,0.0f,1.0f, //5-2
            0.0f,1.0f,0.0f,1.0f,
            0.0f,1.0f,0.0f,1.0f,
            1.0f,0.0f,0.0f,1.0f, //6-1
            1.0f,0.0f,0.0f,1.0f,
            1.0f,0.0f,0.0f,1.0f,
            1.0f,0.0f,0.0f,1.0f, //6-2
            1.0f,0.0f,0.0f,1.0f,
            1.0f,0.0f,0.0f,1.0f
	};

	public Cube(float size) {
        super();

		for (int i = 0; i < vertices.length; i++) {
			vertices[i] = vertices[i] * size/2;
		}

		ByteBuffer vBuff = ByteBuffer.allocateDirect(vertices.length * 4);
		vBuff.order(ByteOrder.nativeOrder());
		verticesBuffer = vBuff.asFloatBuffer();
		
		ByteBuffer nBuff = ByteBuffer.allocateDirect(normals.length * 4);
		nBuff.order(ByteOrder.nativeOrder());
		normalsBuffer = nBuff.asFloatBuffer();
		
		ByteBuffer cBuff = ByteBuffer.allocateDirect(colors.length * 4);
		cBuff.order(ByteOrder.nativeOrder());
		colorsBuffer = cBuff.asFloatBuffer();
					
		verticesBuffer.put(vertices).position(0);
		normalsBuffer.put(normals).position(0);
		colorsBuffer.put(colors).position(0);
	}

	@Override
	public void render(int vertexAttrib, int normalAttrib, int colorAttrib, int modelLocation) {

		if(modelLocation == -1) throw new RuntimeException("Model location is not specified");

		GLES20.glUniformMatrix4fv(modelLocation, 1, false, getModelMatrix(), 0);

		if(vertexAttrib != -1) {

			verticesBuffer.position(0);
			GLES20.glVertexAttribPointer(vertexAttrib, 3, GLES20.GL_FLOAT, false, 0, verticesBuffer);
			GLES20.glEnableVertexAttribArray(vertexAttrib);

			if(colorAttrib != -1){
				colorsBuffer.position(0);
				GLES20.glVertexAttribPointer(colorAttrib, 4, GLES20.GL_FLOAT, false, 0, colorsBuffer);
				GLES20.glEnableVertexAttribArray(colorAttrib);
			}

			if(normalAttrib != -1){
				normalsBuffer.position(0);
				GLES20.glVertexAttribPointer(normalAttrib, 3, GLES20.GL_FLOAT, false, 0, normalsBuffer);
				GLES20.glEnableVertexAttribArray(normalAttrib);
			}

			GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertices.length / 3);

			GLES20.glDisableVertexAttribArray(normalAttrib);
			GLES20.glDisableVertexAttribArray(colorAttrib);
			GLES20.glDisableVertexAttribArray(vertexAttrib);
		}
		else throw new RuntimeException("Vertex attribute is not specified");
	}
}