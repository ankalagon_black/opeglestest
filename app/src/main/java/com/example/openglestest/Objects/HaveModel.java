package com.example.openglestest.Objects;

import android.opengl.Matrix;

/**
 * Created by Константин on 24.12.2016.
 */

public class HaveModel {

    private float[] modelMatrix;

    public HaveModel(){
        modelMatrix = new float[16];
        Matrix.setIdentityM(modelMatrix, 0);
    }

    public void addRotation(float a, float x, float y, float z){
        Matrix.rotateM(modelMatrix, 0, a, x, y, z);
    }

    public void addTranslation(float x, float y, float z){
        Matrix.translateM(modelMatrix, 0, x, y, z);
    }

    public float[] getModelMatrix(){
        return modelMatrix;
    }
}
