package com.example.openglestest;

import android.opengl.GLES20;

import com.example.openglestest.Objects.HaveModel;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by Константин on 25.12.2016.
 */

public class LightQuad extends HaveModel {
    private final FloatBuffer verticesBuffer;

    float[] vertices = {
            0.0f,-0.5f,-0.5f, //1-1
            0.0f,0.5f,-0.5f,
            0.0f,0.5f,0.5f,
            0.0f,-0.5f,-0.5f, //1-2
            0.0f,0.5f,0.5f,
            0.0f,-0.5f,0.5f,
    };

    public LightQuad() {
        super();

        ByteBuffer vBuff = ByteBuffer.allocateDirect(vertices.length * 4);
        vBuff.order(ByteOrder.nativeOrder());
        verticesBuffer = vBuff.asFloatBuffer();
        verticesBuffer.put(vertices).position(0);
    }

    public void render(int vertexAttrib, int MLocation) {

        GLES20.glUniformMatrix4fv(MLocation, 1, false, getModelMatrix(), 0);

        verticesBuffer.position(0);
        GLES20.glVertexAttribPointer(vertexAttrib, 3, GLES20.GL_FLOAT, false, 0, verticesBuffer);
        GLES20.glEnableVertexAttribArray(vertexAttrib);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertices.length / 3);

        GLES20.glDisableVertexAttribArray(vertexAttrib);
    }
}
