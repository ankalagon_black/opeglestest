package com.example.openglestest;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.GLES20;

import com.example.openglestest.Objects.HaveModel;

public class SmallPlane extends HaveModel {
    private final FloatBuffer verticesBuffer;
    private final FloatBuffer normalsBuffer;
    private final FloatBuffer colorsBuffer;

    float[] vertices = {
            -22.0f,0.0f,-22.0f,
            -22.0f,0.0f,22.0f,
            22.0f,0.0f,22.0f,
            -22.0f,0.0f,-22.0f,
            22.0f, 0.0f,22.0f,
            22.0f, 0.0f,-22.0f
    };

    float[] normals = {
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f
    };

    float[] colors = {
            0.5f,0.5f,0.5f,1.0f,
            0.5f,0.5f,0.5f,1.0f,
            0.5f,0.5f,0.5f,1.0f,
            0.5f,0.5f,0.5f,1.0f,
            0.5f,0.5f,0.5f,1.0f,
            0.5f,0.5f,0.5f,1.0f
    };

    public SmallPlane() {
        super();

        ByteBuffer vBuff = ByteBuffer.allocateDirect(vertices.length * 4);
        vBuff.order(ByteOrder.nativeOrder());
        verticesBuffer = vBuff.asFloatBuffer();

        ByteBuffer nBuff = ByteBuffer.allocateDirect(normals.length * 4);
        nBuff.order(ByteOrder.nativeOrder());
        normalsBuffer = nBuff.asFloatBuffer();

        ByteBuffer cBuff = ByteBuffer.allocateDirect(colors.length * 4);
        cBuff.order(ByteOrder.nativeOrder());
        colorsBuffer = cBuff.asFloatBuffer();

        verticesBuffer.put(vertices).position(0);
        normalsBuffer.put(normals).position(0);
        colorsBuffer.put(colors).position(0);
    }

    public void render(int vertexAttrib, int normalAttrib, int colorAttrib, int MLocation, boolean isShadowMapRender) {

        GLES20.glUniformMatrix4fv(MLocation, 1, false, getModelMatrix(), 0);

        verticesBuffer.position(0);
        GLES20.glVertexAttribPointer(vertexAttrib, 3, GLES20.GL_FLOAT, false, 0, verticesBuffer);
        GLES20.glEnableVertexAttribArray(vertexAttrib);

        if (!isShadowMapRender) {
            normalsBuffer.position(0);
            GLES20.glVertexAttribPointer(normalAttrib, 3, GLES20.GL_FLOAT, false, 0, normalsBuffer);
            GLES20.glEnableVertexAttribArray(normalAttrib);

            colorsBuffer.position(0);
            GLES20.glVertexAttribPointer(colorAttrib, 4, GLES20.GL_FLOAT, false, 0, colorsBuffer);
            GLES20.glEnableVertexAttribArray(colorAttrib);
        }

        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 6);

        GLES20.glDisableVertexAttribArray(vertexAttrib);
        GLES20.glDisableVertexAttribArray(normalAttrib);
        GLES20.glDisableVertexAttribArray(colorAttrib);
    }
}
