package com.example.openglestest.FramebufferProperties;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class OpenGLProgram {

	private static final String TAG = "OpenGLProgram";

	private int program;

	private String vertexShader, fragmentShader;

	public OpenGLProgram(int rawVId, int rawFId, Context context) {
		StringBuilder vsBuilder = new StringBuilder();
		StringBuilder fsBuilder = new StringBuilder();

		try {
			InputStream inputStream = context.getResources().openRawResource(rawVId);
			BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
			String read = in.readLine();
			while (read != null) {
				vsBuilder.append(read + "\n");
				read = in.readLine();
			}

			inputStream = context.getResources().openRawResource(rawFId);
			in = new BufferedReader(new InputStreamReader(inputStream));
			read = in.readLine();
			while (read != null) {
				fsBuilder.append(read + "\n");
				read = in.readLine();
			}
		} catch (Exception e) {
			Log.d(TAG, "Could not read shader: " + e.getLocalizedMessage());
		}

		this.vertexShader = vsBuilder.toString();
		this.fragmentShader = fsBuilder.toString();
		if (!createProgram()) throw new RuntimeException("Error at creating shaders");
	}

	private boolean createProgram() {
		int vertexShaderId = createShader(GLES20.GL_VERTEX_SHADER, vertexShader);
		if (vertexShaderId == 0) return false;

		int fragmentShaderId = createShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);
		if (fragmentShaderId == 0) return false;

		program = GLES20.glCreateProgram();
		if (program != 0) {
			GLES20.glAttachShader(program, vertexShaderId);
			GLES20.glAttachShader(program, fragmentShaderId);
			GLES20.glLinkProgram(program);
			int[] linkStatus = new int[1];
			GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, linkStatus, 0);
			if (linkStatus[0] != GLES20.GL_TRUE) {
				Log.e(TAG, "Could not link program: ");
				Log.e(TAG, GLES20.glGetProgramInfoLog(program));
				GLES20.glDeleteProgram(program);
				program = 0;
				return false;
			}
		}
		else Log.d(TAG, "Could not create program");
		return true;
	}

	private int createShader(int shaderType, String code) {
		int shader = GLES20.glCreateShader(shaderType);
		if (shader != 0) {
			GLES20.glShaderSource(shader, code);
			GLES20.glCompileShader(shader);
			int[] compiled = new int[1];
			GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);
			if (compiled[0] != GLES20.GL_TRUE) {
				Log.e(TAG, "Could not compile shader ");
				Log.e(TAG, GLES20.glGetShaderInfoLog(shader));
				GLES20.glDeleteShader(shader);
				shader = 0;
			}
		}
		return shader;
	}

	public int getProgram() {
		return program;
	}
}
