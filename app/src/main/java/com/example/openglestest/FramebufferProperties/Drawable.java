package com.example.openglestest.FramebufferProperties;

/**
 * Created by Константин on 29.12.2016.
 */

public interface Drawable {
    void draw();
}
