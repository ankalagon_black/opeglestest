package com.example.openglestest.FramebufferProperties;

import android.opengl.GLES20;

import com.example.openglestest.Objects.Renderable;

import java.util.List;

/**
 * Created by Константин on 29.12.2016.
 */

public class SimpleFramebufferWrapper extends FrameBufferWrapper {

    public static final String COLOR_ATTRIB = "aColor";

    private int program;

    private int vertexAttrib, colorAttrib;
    private int modelLocation, projViewLocation;

    private float[] projViewMatrix = null;

    public SimpleFramebufferWrapper(int FBOid, OpenGLProgram openGLProgram, List<Renderable> objects) {
        super(FBOid, objects);

        this.program = openGLProgram.getProgram();

        modelLocation = GLES20.glGetUniformLocation(program, M_MATRIX);
        projViewLocation = GLES20.glGetUniformLocation(program, PV_MATRIX);

        vertexAttrib = GLES20.glGetAttribLocation(program, VERTEX_ATTRIB);
        colorAttrib = GLES20.glGetAttribLocation(program, COLOR_ATTRIB);
    }

    @Override
    protected void initializeUniforms() {
        GLES20.glUniformMatrix4fv(projViewLocation, 1, false, projViewMatrix, 0);
    }

    @Override
    protected int getVertexAttrib() {
        return vertexAttrib;
    }

    @Override
    protected int getNormalAttrib() {
        return -1;
    }

    @Override
    protected int getColorAttrib() {
        return colorAttrib;
    }

    @Override
    protected int getModelLocation() {
        return modelLocation;
    }

    @Override
    protected int getProgram() {
        return program;
    }

    public void setProjViewMatrix(float[] projViewMatrix){
        this.projViewMatrix = projViewMatrix;
    }
}
