package com.example.openglestest.FramebufferProperties;

import android.opengl.GLES20;

import com.example.openglestest.Objects.Renderable;

import java.util.List;

/**
 * Created by Константин on 29.12.2016.
 */

public abstract class FrameBufferWrapper implements Drawable{

    public static final String PV_MATRIX = "mProjView";
    public static final String M_MATRIX = "mModel";

    public static final String VERTEX_ATTRIB = "aVertex";

    private int FBOid;

    private int width, height;

    private List<Renderable> objects;

    public FrameBufferWrapper(int FBOid, List<Renderable> objects){
        this.FBOid = FBOid;
        this.objects = objects;
    }

    public FrameBufferWrapper(int FBOid, int width, int height, List<Renderable> objects){
        this.FBOid = FBOid;
        this.width = width;
        this.height = height;
        this.objects = objects;
    }

    public void setWidthAndHeight(int width, int height){
        this.width = width;
        this.height = height;
    }

    protected abstract void initializeUniforms();

    protected abstract int getVertexAttrib();

    protected abstract int getNormalAttrib();

    protected abstract int getColorAttrib();

    protected abstract int getModelLocation();

    protected abstract int getProgram();

    private void drawObjects(){
        for (Renderable i: objects) i.render(getVertexAttrib(), getNormalAttrib(), getColorAttrib(), getModelLocation());
    }

    @Override
    public void draw() {
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, FBOid);
        GLES20.glViewport(0, 0, width, height);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        GLES20.glUseProgram(getProgram());

        initializeUniforms();

        drawObjects();
    }
}
