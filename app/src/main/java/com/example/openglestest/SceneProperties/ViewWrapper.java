package com.example.openglestest.SceneProperties;

import android.opengl.Matrix;

/**
 * Created by Константин on 29.12.2016.
 */

public class ViewWrapper {

    protected float[] viewMatrix = new float[16];

    private float[] unitDirection = new float[3];

    protected float distance;

    public ViewWrapper(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ){
        Matrix.setLookAtM(viewMatrix, 0, eyeX, eyeY, eyeZ, centerX, centerY, centerZ, 0, 1, 0);

        unitDirection[0] = eyeX - centerX;
        unitDirection[1] = eyeY - centerY;
        unitDirection[2] = eyeZ - centerZ;

        distance = (float) Math.sqrt((double) unitDirection[0] * unitDirection[0] + unitDirection[1] * unitDirection[1] + unitDirection[2] * unitDirection[2]);

        unitDirection[0] /= distance;
        unitDirection[1] /= distance;
        unitDirection[2] /= distance;
    }

    public float[] getPosition(){
        float[] result = new float[3];
        for(int i = 0; i < unitDirection.length; i++){
            result[i] = unitDirection[i] * distance;
        }
        return result;
    }

    public float[] getViewMatrix(){
        return viewMatrix;
    }
}
