package com.example.openglestest.SceneProperties;

import android.opengl.Matrix;

/**
 * Created by Константин on 28.12.2016.
 */

public class Camera extends ViewWrapper {

    private float yRotation, zRotation;

    public Camera(float eyeX, float eyeY, float eyeZ) {
        super(eyeX, eyeY, eyeZ, 0, 0, 0);
    }

    public void addYRotation(float angle){
        yRotation += angle;
    }

    public void addZRotation(float angle){
        zRotation += angle;
    }

    public void addDistance(float distance){
        this.distance += distance;
    }

    public float[] getViewMatrix(){
        float[] result = new float[16];
        Matrix.setIdentityM(result, 0);
        float[] position = getPosition();
        Matrix.translateM(result, 0, -position[0], -position[1], -position[2]);
        Matrix.rotateM(result, 0, zRotation, 0, 0, -1);
        Matrix.rotateM(result, 0, yRotation, 0, 1, 0);
        Matrix.multiplyMM(result, 0, viewMatrix, 0, result, 0);
        return result;
    }
}
