package com.example.openglestest.SceneProperties;

import android.opengl.Matrix;

/**
 * Created by Константин on 31.12.2016.
 */

public class ProjectionWrapper {
    private float[] projectionMatrix = new float[16];

    public ProjectionWrapper(int width, int height){
        float ratio = (float) width / (float) height,
                bottom = -1.0f, top = 1.0f, near = 1.0f, far = 10.0f;

        Matrix.frustumM(projectionMatrix, 0, -ratio, ratio, bottom, top, near, far);
    }

    public float[] getProjectionMatrix(){
        return projectionMatrix;
    }
}


