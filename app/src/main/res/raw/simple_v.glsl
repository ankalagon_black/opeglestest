precision highp float;

uniform mat4 mProjView;
uniform mat4 mModel;

attribute vec4 aVertex;
attribute vec4 aColor;

varying vec4 vColor;

void main(){
    vColor = aColor;
    gl_Position = mProjView * mModel * aVertex;
}