precision highp float;

uniform mat4 uPVMatrix;

uniform mat4 uMMatrix;

attribute vec4 aPosition;

varying vec4 vPosition;

void main() {
    vPosition = uPVMatrix * uMMatrix * aPosition;
	gl_Position = vPosition;
}