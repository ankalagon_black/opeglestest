precision highp float;

uniform mat4 uPVMatrix;

uniform mat4 uMMatrix;

attribute vec4 aPosition;

void main() {
	gl_Position = uPVMatrix * uMMatrix * aPosition;
}