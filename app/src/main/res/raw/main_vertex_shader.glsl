precision highp float;

uniform mat4 uPVMatrix;

uniform mat4 uMMatrix;

attribute vec4 aPosition;
attribute vec4 aColor;
attribute vec3 aNormal;

varying vec4 vPosition;
varying vec4 vColor;          		
varying vec3 vNormal;

void main() {
	vPosition = uMMatrix * aPosition;
	vColor = aColor;
	vNormal = mat3(uMMatrix) * aNormal;

	gl_Position = uPVMatrix * vPosition;
}