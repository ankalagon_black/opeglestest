precision highp float;

uniform mat4 uPVMatrix;

uniform mat4 uMMatrix;

uniform vec3 uLightSourcePos;

attribute vec4 aPosition;

varying vec4 vPosition;
varying vec4 vLight;

void main() {
	vPosition = uPVMatrix * uMMatrix * aPosition;
	vLight = uPVMatrix * vec4(uLightSourcePos, 1.0);

	gl_Position = vPosition;
}