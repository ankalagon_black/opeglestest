precision highp float;

uniform sampler2D uOcclusionMap;

uniform sampler2D uColorMap;

varying vec4 vPosition;
varying vec4 vLight;

const int NUM_SAMPLES = 25;
const float EXPOSURE = 0.8;
const float DECAY = 0.87;
const float DENSITY = 1.;
const float WEIGHT = 1.;

void main(){

	vec2 coord = ((vPosition / vPosition.w + 1.) / 2.0).xy;
    vec2 lightCoord = ((vLight / vLight.w + 1.) / 2.0).xy;

    vec2 deltaTexCoord = coord - lightCoord;
    deltaTexCoord *= 1.0 / float(NUM_SAMPLES) * DENSITY;

    vec2 currentCoord = coord;
    float illuminationDecay = 1.0;
    vec4 color = texture2D(uOcclusionMap, coord);
    for(int i=0; i < NUM_SAMPLES ; i++){
        currentCoord -= deltaTexCoord;
        vec4 s = texture2D(uOcclusionMap, currentCoord);
        s *= illuminationDecay * WEIGHT;
        color += s;
        illuminationDecay *= DECAY;
    }

    color = vec4(color.rgb * EXPOSURE, 1.0);

    gl_FragColor = color + texture2D(uColorMap, coord);
}

//	    vec2 coord = ((vPosition / vPosition.w + 1.) / 2.0).xy;
//        vec2 lightCoord = ((vLight / vLight.w + 1.) / 2.0).xy;
//
//        vec2 deltaCoord = (coord - lightCoord) / 40. * 0.6;
//
//        vec3 color = texture2D(uColorMap, coord).rgb;
//    //
//    //    float illDeacy = 1.0;
//    //
//    //    for (int i = 0; i < 40; i++){
//    //        coord -= deltaCoord;
//    //        vec3 sampleC =  texture2D(uColorMap, coord).rgb;
//    //
//    //        sampleC *= illDeacy * 0.2;
//    //
//    //        color += sampleC;
//    //
//    //        illDeacy *= 1.0;
//    //    }
//
//         gl_FragColor = texture2D(uOcclusionMap, coord);

//const int NUM_SAMPLES = 30;
//const float EXPOSURE = 0.0325;
//const float DECAY = 1.0;
//const float DENSITY = 1.0;
//const float WEIGHT = 0.75;
//
//void main(){
//
//	vec2 coord = ((vPosition / vPosition.w + 1.) / 2.0).xy;
//    vec2 lightCoord = ((vLight / vLight.w + 1.) / 2.0).xy;
//
//    float illuminationDecay = 1.0;
//    vec2 deltaTexCoord= coord - lightCoord;
//    deltaTexCoord *= 1.0 / float(NUM_SAMPLES) * DENSITY;
//
//    vec2 currentTexCoord = coord;
//
//    vec4 color = vec4(0);
//
//    for (int i = 0; i < NUM_SAMPLES; i++) {
//        currentTexCoord -= deltaTexCoord;
//        vec4 occlusionSample = texture2D(uOcclusionMap, currentTexCoord);
//        occlusionSample *= illuminationDecay * WEIGHT;
//        color += occlusionSample;
//        illuminationDecay *= DECAY;
//    }
//
//    color *= EXPOSURE;
//
////    color += texture2D(uColorMap, coord);
//
//    gl_FragColor = clamp(color, vec4(0), vec4(1));;
//}