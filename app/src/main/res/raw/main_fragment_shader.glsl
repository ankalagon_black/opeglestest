precision highp float;

uniform vec3 uCameraPos;

uniform mat4 uPVLightMatrix;
uniform sampler2D uShadowMap;
uniform vec3 uLightSourcePos;

varying vec4 vPosition;
varying vec4 vColor;
varying vec3 vNormal;

float unpack (vec4 color)
{
    const vec4 bitShifts = vec4(1.0 / (256.0 * 256.0 * 256.0),
                                1.0 / (256.0 * 256.0),
                                1.0 / 256.0,
                                1);
    return dot(color,bitShifts);
}

float isInShadow(vec4 vertexInLightPV){
    vertexInLightPV = vertexInLightPV / vertexInLightPV.w;
    vertexInLightPV = (vertexInLightPV + 1.0) / 2.0;

    float shadowMapDepth = unpack(texture2D(uShadowMap, vertexInLightPV.xy));

    float bias = 0.007;

    return float(shadowMapDepth > vertexInLightPV.z - bias);
}

void main(){
    vec3 lightColor = vec3(1.,1.,1.);

    vec3 lightVector = normalize(uLightSourcePos - vPosition.xyz);
    float diffuseCoeff  = max(0.0, dot(vNormal, lightVector));
    vec3 diffuse = diffuseCoeff * vColor.rgb * lightColor;

    float ambientCoeff = 0.02;
    vec3 ambient = ambientCoeff * vColor.rgb * lightColor;

    float specularCoefficient = 0.0;
    if(diffuseCoeff > 0.01) specularCoefficient = pow(max(0.0, dot(normalize(uCameraPos - vPosition.xyz), reflect(-lightVector, vNormal))), 0.6);
    vec3 specular = specularCoefficient * vColor.rgb * lightColor;

    float attenuation = 1.0 / (1.0 + 0.07 * pow(length(uLightSourcePos - vPosition.xyz), 2.0));

    vec4 vertexInLightPV = uPVLightMatrix * vPosition;
    float shadowCoeff = 1.0;
    if(vertexInLightPV.w > 0.0){
       shadowCoeff = isInShadow(vertexInLightPV);
       shadowCoeff = (shadowCoeff * 0.5) + 0.5;
    }

    vec3 linearColor = ambient + attenuation * (diffuse + specular);

    vec3 gamma = vec3(1.0/2.2);

    gl_FragColor = vColor;
}
